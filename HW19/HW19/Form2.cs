﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;


namespace HW19
{
    public partial class Form2 : Form
    {

        public Form2()
        {
            InitializeComponent();
        }

        public static class GlobalData
        {
            public static string userName;
            public static int numberOfDates = 0;
            public static string[] lines = new string[50];
            public static string[] names = new string[50];
            public static string[] dates = new string[50];
            public static bool isSorted = false;
        };

        public void DataSort(string userName)
        {
            GlobalData.lines = System.IO.File.ReadAllLines(userName + "BD.txt");
            GlobalData.numberOfDates = GlobalData.lines.Length;
            for (int i = 0; i < GlobalData.numberOfDates; i++)
            {
                GlobalData.names[i] = GlobalData.lines[i].Split(',')[0];
                GlobalData.dates[i] = GlobalData.lines[i].Split(',')[1];
            }
            GlobalData.isSorted = true;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (GlobalData.isSorted == false)
            {
                DataSort(GlobalData.userName);
            }

            //change date format to fit the file dates format for comparison
            string chosenDate = dateTimePicker1.ToString();
            string[] dayMonthYear;
            dayMonthYear = chosenDate.Split(',')[1].Split(' ')[2].Split('/');
            char[] current;
            for(int i = 0; i < 2; i++)
            {
                //delete zero 
                current = dayMonthYear[i].ToArray();
                if(current[0] == '0')
                {
                    dayMonthYear[i] = current[1].ToString();
                }
            }
            chosenDate = dayMonthYear[1] + "/" + dayMonthYear[0] + "/" + dayMonthYear[2];
            for (int i = 0; i < GlobalData.numberOfDates; i++)
            {
                if (chosenDate == GlobalData.dates[i])
                {
                    label1.Text = "In the chosen date, " + GlobalData.names[i] + " is celebrating Birthday !";
                    label1.Show();
                    break;
                }
                else
                {
                    label1.Text = "No one celebrates birthday in the chosen date";
                    label1.Show();
                }
            }
        }

        public string getUserName
        {
            set { GlobalData.userName = value; }
        }

    }
}
