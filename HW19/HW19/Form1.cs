﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;





namespace HW19
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '*';
            textBox2.MaxLength = 50;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            //Check user details validation
            if (textBox1.Text != "Magshim" && textBox1.Text != "Lidor")
            {
                if(textBox2.Text != "123456" && textBox2.Text != "321123")
                {
                    DialogResult result;
                    result = MessageBox.Show("Invalid username or password");  
                }
            }
            else
            {
                //create file if does not exist
                using (StreamWriter w = File.AppendText(textBox1.Text + "BD.txt"))

                //switch forms
                this.Hide();
                form2.getUserName = this.getUserName;
                form2.Closed += (s, args) => this.Close();
                form2.Show();
            }            
        }

        public string getUserName
        {
            get { return textBox1.Text; }
        }
    }
}
